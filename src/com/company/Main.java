package com.company;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        char[][] field = new char[3][3];
        Scanner scanner = new Scanner(System.in);
        boolean winnerX = false;
        boolean winnerO = false;
        print(field);
        while(!winnerO) {
            input(scanner, field);
            print(field);
            winnerX = checkX(field);
            if (winnerX) {
                break;
            }
            System.out.println("Klasse Zug! Der Algorithmus berechnet gerade alle möglichen Ergebnisse...");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            advancedAlgorithms(field);
            print(field);
            winnerO = checkO(field);
        }
        if (winnerX) {
            System.out.println("Du hast gewonnen!");
        } else if (winnerO){
            System.out.println("Du wurdest besiegt!");
        }
    }

    public static void print(char [][] field) {
        System.out.println("2   | " + field[0][2] + " | "+ field[1][2] + " | "+ field[2][2] + " | ");
        System.out.println("1   | " + field[0][1] + " | "+ field[1][1] + " | "+ field[2][1] + " | ");
        System.out.println("0   | " + field[0][0] + " | "+ field[1][0] + " | "+ field[2][0] + " | ");
        System.out.println("      0   1   2");
    }

    public static void input(Scanner scanner, char[][] field) {
        int x;
        int y;
        do {
            System.out.println("An welcher Stelle wollen Sie ihr Kreuz setzen? Geben Sie dies im folgendem Format an: x:y");
            String input = scanner.nextLine();
            String[] parts = input.split(":");
            x = Integer.parseInt(parts[0]);
            y = Integer.parseInt(parts[1]);
        } while (field[x][y] == 'X' || field[x][y] == 'O');
        field[x][y] = 'X';
    }

    public static void advancedAlgorithms(char[][] field){
        int randomX = (int)(Math.random() * 3);
        int randomY = (int)(Math.random() * 3);
        while (field[randomX][randomY] == 'X' || field[randomX][randomY] == 'O') {
            randomX = (int)(Math.random() * 3);
            randomY = (int)(Math.random() * 3);
        }
        field[randomX][randomY] = 'O';
    }

    public static boolean checkX(char[][] field) {
        boolean winner = false;
        int counter = 0;
        for(int x = 0; x < 3; x++) {
            counter = 0;
            for (int y = 0; y < 3; y++) {
                if (field[x][y] == 'X') {
                    counter++;
                    if (counter > 2) {
                        winner = true;
                    }
                }
            }
        }
        for(int y = 0; y < 3; y++) {
            counter = 0;
            for (int x = 0; x < 3; x++) {
                if (field[x][y] == 'X') {
                    counter++;
                    if (counter > 2) {
                        winner = true;
                    }
                }
            }
        }
        if (field[0][0] == 'X' && field[1][1] == 'X' && field[2][2] == 'X') {
            winner = true;
        }
        if (field[0][2] == 'X' && field[1][1] == 'X' && field[2][0] == 'X') {
            winner = true;
        }
        return (winner);
    }

    public static boolean checkO(char[][] field) {
        boolean winner = false;
        int counter = 0;
        for(int x = 0; x < 3; x++) {
            counter = 0;
            for (int y = 0; y < 3; y++) {
                if (field[x][y] == 'O') {
                    counter++;
                    if (counter > 2) {
                        winner = true;
                    }
                }
            }
        }
        for(int y = 0; y < 3; y++) {
            counter = 0;
            for (int x = 0; x < 3; x++) {
                if (field[x][y] == 'O') {
                    counter++;
                    if (counter > 2) {
                        winner = true;
                    }
                }
            }
        }
        if (field[0][0] == 'O' && field[1][1] == 'O' && field[2][2] == 'O') {
            winner = true;
        }
        if (field[0][2] == 'O' && field[1][1] == 'O' && field[2][0] == 'O') {
            winner = true;
        }
        return (winner);
    }
}


